
public class Principal {
	
	public static void main(String[] args)
	{
		int[][] m = new int[][]{
					  {-1,1,0,0,-1,1,0,0},
					  {0,0,1,-1,0,0,1,-1},
					  {-1,0,1,0,0,0,0,0},
					  {1,-1,0,0,0,0,0,0},
					  {0,1,0,-1,0,0,0,0},
					  {0,0,-1,1,0,0,0,0},
					  {0,0,0,0,-1,0,1,0},
					  {0,0,0,0,1,-1,0,0},
					  {0,0,0,0,0,1,0,-1},
					  {0,0,0,0,0,0,-1,1}};
		int[] marcado = new int[] {5,8,15,0,0,0,10,0,0,0};
		int[] S = new int[] {1,0,0,0,1,0,0,0};
		int[] resultado = new int[marcado.length];
		
		if(Multiplicar(m,S,resultado))
		{
			mostarVector(sumar(marcado,resultado));
		}
		
		System.out.println("La matriz de incidencia tiene "+m.length+" filas y "+m[0].length+" columnas");
	}
	
	public static boolean Multiplicar(int[][] a,int[] b, int[] r)
	{
		// Para que se puedan multiplicar, a debe tener tantas columnas como filas tenga b
		boolean correcto = true;
		if(a[0].length != b.length)
		{
			correcto = false;
		}
		else
		{
			for(int i = 0; i < a.length; i++)
			{
				for(int j = 0; j < b.length ; j++)
				{
					r[i] += a[i][j] * b[j];
				}
			}
		}
		return correcto;
	}
	
	public static int[] sumar (int[] a, int[] b)
	{
		int[] r = new int[a.length];
		for(int i = 0; i < a.length; i++)
		{
			r[i] = a[i] + b [i];
		}
		return r;
	}
	
	public static void mostarVector (int[] m)
	{
		for(int i = 0; i < m.length; i++)
		{
			System.out.println(m[i]);
		}
	}
}
